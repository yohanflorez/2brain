var $ = jQuery.noConflict();

if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
    document.getElementsByTagName("body")[0].className += " safari";
}

$('#sliderCarousal').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{  
            items:1
        }
    }
})

$('#CASOScarousal').owlCarousel({
    loop:true,
    margin:10,
    pagination : true,
    nav:true,
    navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})


var a = 0;
$(window).scroll(function() {

    var oTop = $('#counter').offset().top - window.innerHeight;
    if (a == 0 && $(window).scrollTop() > oTop) {
    $('.counter-value').each(function() {
        var $this = $(this),
        countTo = $this.attr('data-count');
        $({
        countNum: $this.text()
        }).animate({
            countNum: countTo
        },

        {

            duration: 3000,
            easing: 'swing',
            step: function() {
            $this.text(Math.floor(this.countNum));
            },
            complete: function() {
            $this.text(this.countNum);
            //alert('finished');
            }

        });
    });
    a = 1;
    }

});

$('#ServiciosCar').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:5
        }
    }
})

$('#NuestrasRedesCar').owlCarousel({
    loop:true,
    margin:0,						stagePadding: 30,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:5
        }
    }
})
$(document).ready(function () {

    $(".navbar-toggle").on("click", function () {
        $(this).toggleClass("active");
    });

    var header = $(".navbar-desktop");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 100) {
            header.addClass("navbar-fixed-top");
        } else {
            header.removeClass('navbar-fixed-top');
        }
    });

    $('.ancla').click(function(){
        var link = $(this);
        var anchor  = link.attr('href');
        $('html, body').stop().animate({
            scrollTop: jQuery(anchor).offset().top - 100
        }, 1200);
        return false;
    });
});

$(".plusICon").click(function(){
    $(".MINKUcAROUSALtxt").show();
    $(".MINKUcAROUSALimg").hide();
});
$(".crossICon").click(function(){
    $(".MINKUcAROUSALtxt").hide();
    $(".MINKUcAROUSALimg").show();
});